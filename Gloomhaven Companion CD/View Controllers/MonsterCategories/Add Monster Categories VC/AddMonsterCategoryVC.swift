//
//  AddMonsterCategoryVC.swift
//  Gloomhaven Companion CD
//
//  Created by Steven McGrath on 9/30/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit
import CoreData

class AddMonsterCategoryVC: UIViewController {

    // MARK - Properties
    var managedObjectContext: NSManagedObjectContext?

    // MARK - Outlets
    @IBOutlet weak var categoryNameTextField: UITextField!

    // Normal Outlets
    @IBOutlet weak var normalHpLabel: UILabel!
    @IBOutlet weak var normalMovementLabel: UILabel!
    @IBOutlet weak var normalAttackLabel: UILabel!
    @IBOutlet weak var normalRangeLabel: UILabel!

    // Elite Outlets
    @IBOutlet weak var eliteHpLabel: UILabel!
    @IBOutlet weak var eliteMovementLabel: UILabel!
    @IBOutlet weak var eliteAttackLabel: UILabel!
    @IBOutlet weak var eliteRangeLabel: UILabel!

    // MARK - Actions

    // Normal Actions
    @IBAction func increaseNormalHp(_ sender: UIButton) {
        increaseStat(for: normalHpLabel)
    }
    @IBAction func decreaseNormalHp(_ sender: UIButton) {
        decreaseStat(for: normalHpLabel)
    }
    @IBAction func increaseNormalMovement(_ sender: UIButton) {
        increaseStat(for: normalMovementLabel)
    }
    @IBAction func decreaseNormalMovement(_ sender: UIButton) {
        decreaseStat(for: normalMovementLabel)
    }
    @IBAction func increaseNormalAttack(_ sender: UIButton) {
        increaseStat(for: normalAttackLabel)
    }
    @IBAction func decreaseNormalAttack(_ sender: UIButton) {
        decreaseStat(for: normalAttackLabel)
    }
    @IBAction func increaseNormalRange(_ sender: UIButton) {
        increaseStat(for: normalRangeLabel)
    }
    @IBAction func decreaseNormalRange(_ sender: UIButton) {
        decreaseStat(for: normalRangeLabel)
    }

    // Elite Actions
    @IBAction func increaseEliteHp(_ sender: UIButton) {
        increaseStat(for: eliteHpLabel)
    }
    @IBAction func decreaseEliteHp(_ sender: UIButton) {
        decreaseStat(for: eliteHpLabel)
    }
    @IBAction func increaseEliteMovement(_ sender: UIButton) {
        increaseStat(for: eliteMovementLabel)
    }
    @IBAction func decreaseEliteMovement(_ sender: UIButton) {
        decreaseStat(for: eliteMovementLabel)
    }
    @IBAction func increaseEliteAttack(_ sender: UIButton) {
        increaseStat(for: eliteAttackLabel)
    }
    @IBAction func decreaseEliteAttack(_ sender: UIButton) {
        decreaseStat(for: eliteAttackLabel)
    }
    @IBAction func increaseEliteRange(_ sender: UIButton) {
        increaseStat(for: eliteRangeLabel)
    }
    @IBAction func decreaseEliteRange(_ sender: UIButton) {
        increaseStat(for: eliteRangeLabel)
    }

    // Create Category
    @IBAction func save(_ sender: UIButton) {
        guard let managedObjectContext = managedObjectContext else { return }

        // Validate name and HP fields are properly populated
        guard let name = categoryNameTextField.text, !name.isEmpty else {
            showAlert(with: "Name missing", and: "Your category requires a name")
            return
        }
        guard let normalHpValue = normalHpLabel.text, let eliteHpValue = eliteHpLabel.text, Int(normalHpValue) != 0, Int(eliteHpValue) != 0  else {
            showAlert(with: "HP too low", and: "Your category must have a non zero hp value")
            return
        }
        // Create Monster Category
        let monsterCategory = MonsterCategory(context: managedObjectContext)

        // Create Stats

        let normalHp = Stat(context: managedObjectContext)
        normalHp.name = "normalHp"
        normalHp.value = Int16(normalHpLabel.text!)!

        let normalMovement = Stat(context: managedObjectContext)
        normalMovement.name = "normalMovement"
        normalMovement.value = Int16(normalMovementLabel.text!)!

        let normalAttack = Stat(context: managedObjectContext)
        normalAttack.name = "normalAttack"
        normalAttack.value = Int16(normalAttackLabel.text!)!

        let normalRange = Stat(context: managedObjectContext)
        normalRange.name = "normalRange"
        normalRange.value = Int16(normalRangeLabel.text!)!

        let eliteHp = Stat(context: managedObjectContext)
        eliteHp.name = "eliteHp"
        eliteHp.value = Int16(eliteHpLabel.text!)!

        let eliteMovement = Stat(context: managedObjectContext)
        eliteMovement.name = "eliteMovement"
        eliteMovement.value = Int16(eliteMovementLabel.text!)!

        let eliteAttack = Stat(context: managedObjectContext)
        eliteAttack.name = "eliteAttack"
        eliteAttack.value = Int16(eliteAttackLabel.text!)!

        let eliteRange = Stat(context: managedObjectContext)
        eliteRange.name = "eliteRange"
        eliteRange.value = Int16(eliteRangeLabel.text!)!

        // Configure Cateogry
        monsterCategory.name = categoryNameTextField.text
        monsterCategory.currentUnits = 0
        monsterCategory.maxUnits = 10
        monsterCategory.addToStats(normalHp)
        monsterCategory.addToStats(normalMovement)
        monsterCategory.addToStats(normalAttack)
        monsterCategory.addToStats(normalRange)
        monsterCategory.addToStats(eliteHp)
        monsterCategory.addToStats(eliteMovement)
        monsterCategory.addToStats(eliteAttack)
        monsterCategory.addToStats(eliteRange)

        // Dismiss View Controller

        dismiss(animated: true, completion: nil)
    }


    // Helper functions

    private func increaseStat(for statLabel: UILabel) {
        guard let stat = statLabel.text else { return }
        guard var statAsInt = Int(stat) else { return }
        statAsInt += 1
        statLabel.text = String(statAsInt)
    }

    private func decreaseStat(for statLabel: UILabel) {
        guard let stat = statLabel.text else { return }
        guard var statAsInt = Int(stat) else { return }
        if statAsInt > 0 {
            statAsInt -= 1
        }
        statLabel.text = String(statAsInt)
    }

    // MARK - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Create Monster Category"
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        categoryNameTextField.becomeFirstResponder()
    }
}
