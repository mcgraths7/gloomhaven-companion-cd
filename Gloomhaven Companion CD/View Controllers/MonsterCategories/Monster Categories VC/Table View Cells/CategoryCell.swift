//
//  CategoryCell.swift
//  Gloomhaven Companion CD
//
//  Created by Steven McGrath on 9/30/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    // MARK - Static Properties
    static let reuseIdentifier = "CategoryCell"

    // MARK - Properties

    @IBOutlet weak var categoryNameLabel: UILabel!
    @IBOutlet weak var currentUnitsLabel: UILabel!
    @IBOutlet weak var maxUnitsLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
