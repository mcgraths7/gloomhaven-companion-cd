//
//  ViewController.swift
//  Gloomhaven Companion CD
//
//  Created by Steven McGrath on 9/30/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit
import CoreData

class MonsterCategoriesVC: UIViewController {

    // MARK - Segue Identifiers
    private enum Segue {
        static let Monsters = "Monsters"
        static let AddMonsterCategory = "AddMonsterCategory"
    }

    // MARK - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var monsterCategoriesView: UIView!

    private var coreDataManager = CoreDataManager(modelName: "GloomhavenCompanion")

    private var hasMonsterCategories: Bool {
        guard let fetchedObjects = fetchedResultsController.fetchedObjects else { return false }
        return fetchedObjects.count > 0
    }

    // MARK - Fetched Results Controller setup

    private lazy var fetchedResultsController: NSFetchedResultsController<MonsterCategory> = {
        // Create fetch request
        let fetchRequest: NSFetchRequest<MonsterCategory> = MonsterCategory.fetchRequest()

        // Configure fetch request
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: #keyPath(MonsterCategory.name), ascending: false)]

        // Create fetched results controller
        let fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
                                                                  managedObjectContext: self.coreDataManager.managedObjectContext,
                                                                  sectionNameKeyPath: nil,
                                                                  cacheName: nil)

        // Configure Fetched results controller
        fetchedResultsController.delegate = self

        return fetchedResultsController
    }()

    // MARK - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else { return }

        switch identifier {
        case Segue.AddMonsterCategory:
            guard let destination = segue.destination as? AddMonsterCategoryVC else { return }
            // Configure destination
            destination.managedObjectContext = coreDataManager.managedObjectContext
        default:
            break
        }
    }

    // MARK - View Methods

    private func setupView() {
        setupMessageLabel()
        setupTableView()
    }

    private func updateView() {
        tableView.isHidden = !hasMonsterCategories
        messageLabel.isHidden = hasMonsterCategories
        if let objects = fetchedResultsController.fetchedObjects {
            print(objects)
        }
    }

    private func setupTableView() {
        tableView.isHidden = true
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableView.automaticDimension
        tableView.delegate = self
        tableView.dataSource = self
    }

    private func setupMessageLabel() {
        messageLabel.text = "You don't have any categories yet..."
    }

    // MARK - Helper Methods

    private func fetchNotes() {
        print("!")
        do {
            try fetchedResultsController.performFetch()
        } catch {
            print("Unable to perform fetch request")
            print("\(error), \(error.localizedDescription)")
        }
    }

    // MARK - Lifecycle methods

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Monster Categories"

        setupView()
        fetchNotes()
        updateView()
    }
}

extension MonsterCategoriesVC: NSFetchedResultsControllerDelegate {
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.beginUpdates()
    }

    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        tableView.endUpdates()

        updateView()
    }

    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch (type) {
        case .insert:
            if let indexPath = newIndexPath {
                tableView.insertRows(at: [indexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath, let cell = tableView.cellForRow(at: indexPath) as? CategoryCell {
                configure(cell, at: indexPath)
            }
        case .move:
            if let indexPath = indexPath {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }

            if let newIndexPath = newIndexPath {
                tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        }
    }
}

extension MonsterCategoriesVC: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        guard let sections = fetchedResultsController.sections else { return 0 }
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = fetchedResultsController.sections?[section] else { return 0 }
        return section.numberOfObjects
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        // Dequeue Reusable cell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.reuseIdentifier, for: indexPath) as? CategoryCell else {
            fatalError("Unexpected index path")
        }

        configure(cell, at: indexPath)

        return cell
    }

    private func configure(_ cell: CategoryCell, at indexPath: IndexPath) {
        // Fetch category
        let monsterCategory = fetchedResultsController.object(at: indexPath)

        // Configure cell
        cell.categoryNameLabel.text = monsterCategory.name
        cell.maxUnitsLabel.text = String(monsterCategory.maxUnits)
        cell.currentUnitsLabel.text = String(monsterCategory.currentUnits)
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }

        // Fetch Monster Category
        let monsterCateogry = fetchedResultsController.object(at: indexPath)

        // Delete Category
        coreDataManager.managedObjectContext.delete(monsterCateogry)
    }

}

extension MonsterCategoriesVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
