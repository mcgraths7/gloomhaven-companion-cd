//
//  UIViewController.swift
//  Gloomhaven Companion CD
//
//  Created by Steven McGrath on 9/30/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit

extension UIViewController {

    // MARK - Alerts
    func showAlert(with title: String, and message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alertController.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))

        present(alertController, animated: true, completion: nil)
    }

}
