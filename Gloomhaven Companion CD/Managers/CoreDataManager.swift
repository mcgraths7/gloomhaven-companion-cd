//
//  CoreDataManager.swift
//  Gloomhaven Companion CD
//
//  Created by Steven McGrath on 9/30/18.
//  Copyright © 2018 Steven McGrath. All rights reserved.
//

import UIKit
import CoreData

final class CoreDataManager {

    // MARK - Properties

    private let modelName: String

    // MARK - Setup Core Data Components

    private(set) lazy var managedObjectContext: NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)

        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator

        return managedObjectContext
    }()

    private lazy var managedObjectModel: NSManagedObjectModel = {
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else {
            fatalError("Unable to find Data Model")
        }

        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Unable to load Data Model")
        }

        return managedObjectModel
    }()

    private lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)

        let fileManager = FileManager.default
        let storeName = "\(self.modelName).sqlite"

        let documentDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]

        let persistentStoreURL = documentDirectoryURL.appendingPathComponent(storeName)

        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                                                              configurationName: nil,
                                                              at: persistentStoreURL,
                                                              options: nil)
        } catch {
            fatalError("Unable to add persistent store")
        }

        return persistentStoreCoordinator
    }()

    // MARK - Initialization
    init(modelName: String) {
        self.modelName = modelName

        setupNotificationHandling()
    }

    // MARK - Setup Notification Handling

    private func setupNotificationHandling() {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self,
                                       selector: #selector(saveChanges(_:)),
                                       name: UIApplication.willTerminateNotification,
                                       object: nil)
        notificationCenter.addObserver(self,
                                       selector: #selector(saveChanges(_:)),
                                       name: UIApplication.didEnterBackgroundNotification,
                                       object: nil)
    }

    @objc private func saveChanges(_ notification: Notification) {
        saveChanges()
    }

    private func saveChanges() {
        guard managedObjectContext.hasChanges else { return }

        do {
            try managedObjectContext.save()
        } catch {
            print("Unable to save managed object context:")
            print("\(error), \(error.localizedDescription)")
        }
    }
}
